const _ = require('lodash');
const moment = require('moment-timezone');
const influx = require('influx');

const prodHost = 'influxdb1.iosense.io';
const prodDB = 'prod1_0';

const Sinflux = new influx.InfluxDB({
    host: prodHost,
    database: prodDB
});

const devices = ['CLEM_PN_A1', 'CLEM_PN_A2', 'CLEM_PN_A3', 'CLEM_PN_A4', 'CLEM_PN_A5', 'CLEM_PN_A8', 'CLEM_PN_A6', 'CLEM_PN_A7', 'CLEM_PN_A9', 'CLEM_PN_B1', 'CLEM_PN_B2', 'CLEM_PN_B3', 'CLEM_PN_B4', 'CLEM_PN_B5', 'CLEM_PN_B6', 'CLEM_PN_B7', 'CLEM_PN_B8', 'CLEM_PN_B9', 'CLEM_PN_B10', 'CLEM_PN_B11', 'CLEM_PN_B25', 'CLEM_PN_C3', 'CLEM_PN_C4', 'CLEM_PN_C5', 'CLEM_PN_C6', 'CLEM_PN_C7', 'CLEM_PN_C8', 'CLEM_PN_C9', 'CLEM_PN_D1', 'CLEM_PN_D2', 'CLEM_PN_D3', 'CLEM_PN_D4', 'CLEM_PN_D5', 'CLEM_PN_D6', 'CLEM_PN_D7', 'CLEM_PN_D8', 'CLEM_KH_A1', 'CLEM_KH_A2', 'CLEM_KH_A3', 'CLEM_KH_A4', 'CLEM_KH_A5', 'CLEM_KH_A6', 'CLEM_KH_A7', 'CLEM_KH_A8', 'CLEM_KH_A9', 'CLEM_KH_A10', 'CLEM_KH_A11', 'CLEM_KH_A12', 'CLEM_AB_A1', 'CLEM_AB_A2', 'CLEM_AB_A3', 'CLEM_AB_A4', 'CLEM_AB_A5', 'CLEM_AB_A6', 'CLEM_AB_A7', 'CLEM_AB_A8', 'CLEM_AB_A9', 'CLEM_AB_A10', 'CLEM_AB_A11', 'CLEM_RE_A1', 'CLEM_RE_A2', 'CLEM_RE_A3', 'CLEM_RE_A4', 'CLEM_RE_B1', 'CLEM_RE_B2', 'CLEM_RE_B3', 'CLEM_RE_C1', 'CLEM_RE_C2', 'CLEM_RE_C3', 'CLEM_RE_C4', 'CLEM_RE_C5', 'CLEM_RE_C6', 'CLEM_RE_C7', 'CLEM_RE_C8', 'CLEM_RE_C9', 'CLEM_RE_C10', 'CLEM_RE_C11', 'CLEM_RE_C12', 'CLEM_TD_B1', 'CLEM_TD_B2', 'CLEM_TD_B3', 'CLEM_TD_B4', 'CLEM_TD_B5', 'CLEM_TD_B6', 'CLEM_TD_B7', 'CLEM_TD_B8', 'CLEM_TD_B9', 'CLEM_TD_B10', 'CLEM_TD_B11', 'CLEM_TD_B12', 'CLEM_TD_B13', 'CLEM_TD_B14', 'CLEM_TD_B15', 'CLEM_TD_B16', 'CLEM_TD_C1', 'CLEM_TD_C2', 'CLEM_TD_C3', 'CLEM_TD_C4', 'CLEM_TD_C5', 'CLEM_TD_A1', 'CLEM_TD_A2', 'CLEM_TD_A3', 'CLEM_TD_A4', 'CLEM_TD_A5', 'CLEM_TD_A6', 'CLEM_TD_A7', 'CLEM_TD_A8', 'CLEM_TD_A9', 'CLEM_TD_A10', 'CLEM_TD_A11', 'CLEM_BJ_A1', 'CLEM_BJ_A2', 'CLEM_BJ_A3', 'CLEM_BJ_A4', 'CLEM_BJ_A5', 'CLEM_BJ_A6', 'CLEM_BJ_A7', 'CLEM_BJ_A8', 'CLEM_BJ_A9', 'CLEM_BJ_A10', 'CLEM_BJ_A11', 'CLEM_BJ_A12', 'CLEM_BJ_B1', 'CLEM_BJ_B2', 'CLEM_BJ_B3', 'CLEM_BJ_B4', 'CLEM_BJ_B5', 'CLEM_MT_A1', 'CLEM_MT_B1', 'CLEM_MT_B2', 'CLEM_MT_B3', 'CLEM_MT_B4', 'CLEM_MT_B5', 'CLEM_MT_B6', 'CLEM_BT_A1', 'CLEM_NJ_A1', 'CLEM_SN_A1', 'CLEM_BH_A1', 'CLEM_UN_A', 'CLEM_MR_A1', 'CLEM_RK_A1', 'CLEM_JN_A1', 'CLEM_JN_B', 'CLEM_NH_A', 'CLEM_SMPN_A1' ];


const EMSensors = ['D30', 'D66', 'D67', 'D68', 'D69', 'D70', 'D71', 'D72', 'D29', 'D73', 'D74', 'D75'];
const EMProcessedSensors = ['P30', 'P66', 'P67', 'P68', 'P69', 'P70', 'P71', 'P72', 'P29', 'P73', 'P74', 'P75'];
const EMOffsetSensors = ['OF30', 'OF66', 'OF67', 'OF68', 'OF69', 'OF70', 'OF71', 'OF72', 'OF29', 'OF73', 'OF74', 'OF75'];

const sleep = (ms) =>
    new Promise(resolve => setTimeout(resolve, ms));

const fetchDataFromInfluxAndPublish = async ( devID, sensors, processedSensors, offsetSensors, startTime, endTime, batchCount, publishLimit) => {
    try {
        let isFinished = false;
        let cursor = startTime;

        let sensorsQuery = '';
        for (let i = 0; i < sensors.length; i++) {
            if ( i == 0 ) {
                sensorsQuery = sensorsQuery + `sensor='${sensors[i]}'`;
            } else {
                sensorsQuery = sensorsQuery + ` OR sensor='${sensors[i]}'`;
            }
        }
        if (sensors.length > 0)
            sensorsQuery = `(${sensorsQuery}) and `;
        
        do {
            console.log('************data coming in batches**********');
            let query = `select * from "${devID}" where ${sensorsQuery} time<=${endTime}000000 and time>${cursor}000000 order by time limit ${batchCount}`;
            console.log({ query });
            let data = await Sinflux.query(query);
            isFinished = false;

            // console.log(JSON.stringify(data));            
            if (data.length == 0) isFinished = true;

            let dataArray = [];
            let noOfPoints = 0;

            for (let j = 0; j < data.length; j++) {
                const device = devID;
                const time = parseInt(String(data[j].time.getNanoTime()).slice(0, 13));
                const value = data[j].value;
                const sensor = data[j].sensor;
                cursor = time;

                if (time === undefined || value === undefined || sensor === undefined) {
                    console.log('Packet not valid');
                    console.log({ time, value, sensor});
                    continue;
                }

                let index = sensors.indexOf(sensor);
                if (!index && index !== 0)
                    continue;

                const pSensor = processedSensors[index];

                dataArray.push({
                    measurement: device,
                    tags: { sensor : pSensor},
                    fields: { del : 0, value },
                    timestamp: time,
                });

                if (dataArray.length >= publishLimit) {
                    noOfPoints += (dataArray.length);
                    await Sinflux
                        .writePoints(dataArray, { precision : 'ms' })
                        .then(() => {
                            console.log("Added");
                        })
                        .catch((error) => {
                            console.log("Error");
                        });
                    // console.log(JSON.stringify(dataArray));
                    dataArray = [];
                    await sleep(500);
                }
            }

            if (dataArray.length > 0) {
                noOfPoints += (dataArray.length);
                await Sinflux
                        .writePoints(dataArray, { precision : 'ms'})
                        .then(() => {
                            console.log("Added");
                        })
                        .catch((error) => {
                            console.log("Error");
                        });
                // console.log(JSON.stringify(dataArray));
                dataArray = [];
                await sleep(500);
            }

            console.log({ devID , dpProcessed : noOfPoints });
            console.log('*********isFinished******', isFinished);
            await sleep(1000);
        } while (!isFinished);

    } catch (error) {
        console.log(error);   
    }
}

setTimeout(async () => {
    // 31 december 2022 00:00 IST -> 1672425000000
    // 19 july 2023 00:00 IST -> 1689705000000
    // 19 july 2023 23:00 IST -> 1689787800000
    // 20 july 2023 00:00 IST -> 1689791400000
    const startTime = 1672425000000;
    const endTime = 1689791400000;
    const limit = 25000;
    const publishLimit = 5000;

    for (let i = 0; i < devices.length; i++) {
        // await fetchDataFromInfluxAndPublish(devices[i], EMSensors, EMProcessedSensors, EMOffsetSensors, startTime, endTime, limit, publishLimit);
        console.log(`${i + 1} devices done out of ${devices.length}`);
        await sleep(5000);
    }
    process.exit();
}, 1000);