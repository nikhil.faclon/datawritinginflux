const influx = require('influx');

const prodHost = 'influxdb1.iosense.io';
const prodDB = 'prod1_0';

const Sinflux = new influx.InfluxDB({
    host: prodHost,
    database: prodDB
});

// const devices = [];
// const sensorsToDelete = ['P30', 'P66', 'P67', 'P68', 'P69', 'P70', 'P71', 'P72', 'P29', 'P73', 'P74', 'P75', 'OF30', 'OF66', 'OF67', 'OF68', 'OF69', 'OF70', 'OF71', 'OF72', 'OF29', 'OF73', 'OF74', 'OF75'];

const sleep = (ms) =>
    new Promise(resolve => setTimeout(resolve, ms));

const dataDeleteFromDevice = async (device, sensors, startTime, endTime) => {
    try {
        let sensorsQuery = '';
        for (let i = 0; i < sensors.length; i++) {
            if ( i == 0 ) {
                sensorsQuery = sensorsQuery + `sensor='${sensors[i]}'`;
            } else {
                sensorsQuery = sensorsQuery + ` OR sensor='${sensors[i]}'`;
            }
        }
        if (sensors.length > 0)
            sensorsQuery = `(${sensorsQuery}) and `;
    
        let query = `delete from "${device}" where ${sensorsQuery} time<=${endTime}000000 and time>${startTime}000000`;
        console.log({ query });
        // await Sinflux.query(query);
    } catch (error) {
        console.log(error);
        throw error;
    }
};


setTimeout(async () => {
    // 1 july 2023 00:00:00 IST -> 1688149800000
    // 1 August 2023 00:00:00 IST -> 1690828200000
    const startTime = 1688149800000;
    const endTime = 1690828200000;

    for (let i = 0; i < devices.length; i++) {
        // await dataDeleteFromDevice(devices[i], sensorsToDelete, startTime, endTime);
        console.log(`${i + 1} devices done out of ${devices.length}`);
        await sleep(5000);
    }
    process.exit();
}, 1000);